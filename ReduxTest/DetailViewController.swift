//
//  DetailViewController.swift
//  ReduxTest
//
//  Created by Vlad on 8/28/20.
//

import UIKit
import ReSwift

class DetailViewController: UIViewController, StoreSubscriber {
    @IBOutlet weak var counterLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subcription in
            subcription.select { state in state.counterState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
    
    func newState(state: CounterState) {
        counterLabel.text = "\(state.count)"
    }
}
