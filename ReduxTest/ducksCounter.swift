//
//  ducksCounter.swift
//  ReduxTest
//
//  Created by Vlad on 8/28/20.
//

import ReSwift

// Actions
struct IncrementCntAction: Action { }
struct DecrementCntAction: Action { }

// State
struct CounterState: StateType {
    var count: Int = 0
}

// Reducer
func counterReducer(action: Action, state: CounterState?) -> CounterState {
    var state = state ?? initialCounterState()
    
    switch action {
    case _ as IncrementCntAction: state.count += 1
    case _ as DecrementCntAction: state.count -= 1
    default: break
    }
    
    return state
}

fileprivate func initialCounterState() -> CounterState {
    return CounterState(count: 0)
}
