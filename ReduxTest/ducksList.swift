//
//  ducksList.swift
//  ReduxTest
//
//  Created by Vlad on 8/28/20.
//

import ReSwift
import ReSwiftThunk

// Actions
struct StartListAction: Action {}
struct SuccesListAction: Action {}
struct ErrorListAction: Action {}
let getListAction = Thunk<State> { dispatch, getState in
    dispatch(StartListAction())
    
    DispatchQueue.global().async {
        sleep(2)
        
        DispatchQueue.main.async {
            dispatch(SuccesListAction())
        }
    }
}

// State
struct ListState: StateType {
    var isStartRequest = false
    var isSuccessRequest = false
    var isErrorRequest = false
    var list: [String: String] = [:]
}

// Reducer
func listReducer(action: Action, state: ListState?) -> ListState {
    var state = state ?? initialState()
    
    switch action {
    case _ as StartListAction:
        state.isStartRequest = true
        state.isSuccessRequest = false
        state.isErrorRequest = false
        state.list = [:]
    case _ as SuccesListAction:
        state.isStartRequest = false
        state.isSuccessRequest = true
        state.isErrorRequest = false
        state.list = [:]
    case _ as ErrorListAction:
        state.isStartRequest = false
        state.isSuccessRequest = false
        state.isErrorRequest = true
        state.list = [:]
    default: break
    }
    
    return state
}

fileprivate func initialState() -> ListState {
    return ListState(
        isStartRequest: false,
        isSuccessRequest: false,
        isErrorRequest: false,
        list: [:]
    )
}
