//
//  ViewController.swift
//  ReduxTest
//
//  Created by Vlad on 8/28/20.
//

import UIKit
import ReSwift

class ViewController: UIViewController, StoreSubscriber {
    @IBOutlet weak var cntLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        store.subscribe(self)
        
//        store.subscribe(self) { subcription in
//            subcription.select { state in state.listState }
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
    
    func newState(state: State) {
        cntLabel.text = "\(state.counterState.count)"
    }

    @IBAction func actionUp(_ sender: Any) {
        store.dispatch(IncrementCntAction())
    }
    
    @IBAction func actionDown(_ sender: Any) {
        store.dispatch(DecrementCntAction())
    }
    
    @IBAction func handleGetList(_ sender: Any) {
        store.dispatch(getListAction)
    }
}

