//
//  AppState.swift
//  ReduxTest
//
//  Created by Vlad on 8/28/20.
//

import ReSwift
import ReSwiftThunk

// Global state
struct State: StateType {
    var counterState: CounterState
    var listState: ListState
}

// Global reducer
func appReducer(action: Action, state: State?) -> State {
    return State(
        counterState: counterReducer(action: action, state: state?.counterState),
        listState: listReducer(action: action, state: state?.listState)
    )
}

// Store
let store = Store(
    reducer: appReducer,
    state: nil,
    middleware: [thunkMiddleware, logMiddleware],
    automaticallySkipsRepeats: true
)

// Middlewares
// Thunk
let thunkMiddleware: Middleware<State> = createThunkMiddleware()

// Print to console
fileprivate let logMiddleware: Middleware<State> = { dispatch, getState in
    return { next in
        return { action in
            print("--- RESWIFT ---")
            print("")
            print("Dispatching - \(action)")
            
            let result: Void = next(action)
            
            guard let state = getState() else { return next(action) }
            
            print("Next state: \(state)")
            print("")
            
            return result
        }
    }
}
